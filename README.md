Common Lisp Lambda List Parsing -- CLLLP
========================================

Copyright (c) 2008-2022 Marco Antoniotti
See file COPYING for licensing information


Description
-----------

This library contains a stand-alone parser for **Common Lisp** *Lambda
Lists* (of various kinds).  The parser recognizes the standard lambda
list kinds described in the ANSI standard.  It is also somewhat
extensible to accommodate other kinds of lambda lists that may be
provided by some DSL built on top of Common Lisp.


Installation
------------

**ASDF** and **MK:DESFSYTEM** files are of course provided.
Submission to **quicklisp** is under way.



A Note on Forking
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy

Marco Antoniotti 2022-01-03

