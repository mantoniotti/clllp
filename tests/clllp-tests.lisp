;;;; -*- Mode: Lisp -*-

;;;; clllp-tests.lisp
;;;; Tests fot the Common Lisp Lambda Lisp Parsing library.
;;;;
;;;; See file COPYING copyright and licensing information.

(in-package "CLLLP")

(use-package "FIVEAM")


(def-suite clllp-tests
           :description
           "Test suite (5AM) for the CLLLP library.")


(def-suite ordinary-ll-tests
           :in clllp-tests
           :description
           "Test for ordinary lambda lists. Cfr. ANSI Section 3.4.1.6.")


(in-suite ordinary-ll-tests)

(defparameter oll ())

(def-fixture parse-oll (ll)
  (let ((oll (parse-ll :ordinary ll)))
    (&body)))


#|
(test parse-empty-oll
  (finishes (setf oll (parse-ll :ordinary ()))))
|#


(defparameter *empty-oll* ())

(test parse-empty-oll
  (with-fixture parse-oll (*empty-oll*)
    (pass "Parsing empty ordinary lambda list.")))


(test (is-ordinary-empty-oll :depends-on parse-empty-oll)
  (with-fixture parse-oll (*empty-oll*)
    (is-true (ordinary-lambda-list-p oll))))


(test (empty-oll-check :depends-on is-ordinary-empty-oll)
  (with-fixture parse-oll (*empty-oll*)
    (is-true (null (oll-ordinary-vars oll)))
    (is-true (null (oll-optional-vars oll)))
    (is-true (null (oll-rest-var oll)))
    (is-true (null (oll-keyword-vars oll)))
    (is-true (null (oll-allow-other-keys oll)))
    (is-true (null (oll-auxiliary-vars oll)))
    ))


(defparameter *a-b-oll* '(a b))

(test parse-a-b-oll
  (with-fixture parse-oll (*a-b-oll*)
    (pass "Parsing (a b) ordinary lambda list.")))


(test (is-ordinary-a-b-oll :depends-on parse-a-b-oll)
  (with-fixture parse-oll (*a-b-oll*)
    (is-true (ordinary-lambda-list-p oll))))


(test (a-b-oll-check :depends-on parse-a-b-oll)
  (with-fixture parse-oll (*a-b-oll*)
    (is-false (null (oll-ordinary-vars oll)))
    (is-true (null (oll-optional-vars oll)))
    (is-true (null (oll-rest-var oll)))
    (is-true (null (oll-keyword-vars oll)))
    (is-true (null (oll-allow-other-keys oll)))
    (is-true (null (oll-auxiliary-vars oll)))
    ))

(test (a-b-oll-check-vars :depends-on a-b-oll-check)
  (with-fixture parse-oll (*a-b-oll*)
    (is (equal *a-b-oll* (ll-vars oll)))))


(defparameter *a-&opt-b-oll* '(a &optional (b 42)))

(test parse-a-&opt-b-oll
  (with-fixture parse-oll (*a-&opt-b-oll*)
    (pass "Parsing (a &optional (b 42)) ordinary lambda list.")))

(test (is-ordinary-a-&opt-b-oll :depends-on parse-a-&opt-b-oll)
  (with-fixture parse-oll (*a-&opt-b-oll*)
    (is-true (ordinary-lambda-list-p oll))))

(test (a-&opt-b-oll-check :depends-on is-ordinary-a-&opt-b-oll)
  (with-fixture parse-oll (*a-&opt-b-oll*)
    (is-false (null (oll-ordinary-vars oll)))
    (is-false (null (oll-optional-vars oll)))
    (is-true (null (oll-rest-var oll)))
    (is-true (null (oll-keyword-vars oll)))
    (is-true (null (oll-allow-other-keys oll)))
    (is-true (null (oll-auxiliary-vars oll)))
    ))

(test (a-&opt-b-oll-check-vars :depends-on a-&opt-b-oll-check)
  (with-fixture parse-oll (*a-&opt-b-oll*)
    (is (equal '(a b) (ll-vars oll)))))

(test (a-&opt-b-oll-check-opt-vars :depends-on a-&opt-b-oll-check)
  (with-fixture parse-oll (*a-&opt-b-oll*)
    (is (equal '(b) (mapcar #'llv-name (oll-optional-vars oll))))))

(test (a-&opt-b-oll-check-opt-forms :depends-on a-&opt-b-oll-check)
  (with-fixture parse-oll (*a-&opt-b-oll*)
    ;; (print (ll-default-forms oll))
    (is (equal '(nil (b 42)) (ll-default-forms oll)))))


(defparameter *&opt-a-c-&rest-x-oll* '(&optional (a 2 b) (c 3 d) &rest x))

(test parse-&opt-a-b-&rest-x-oll
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (pass "Parsing (&optional (a 2 b) (c 3 d) &rest x) ordinary lambda list.")))


(test (is-ordinary-&opt-a-b-&rest-x-oll :depends-on parse-&opt-a-b-&rest-x-oll)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (is-true (ordinary-lambda-list-p oll))))

(test (&opt-a-b-&rest-x-oll-check :depends-on is-ordinary-&opt-a-b-&rest-x-oll)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (is-true (null (oll-ordinary-vars oll)))
    (is-false (null (oll-optional-vars oll)))
    (is-false (null (oll-rest-var oll)))
    (is-true (null (oll-keyword-vars oll)))
    (is-true (null (oll-allow-other-keys oll)))
    (is-true (null (oll-auxiliary-vars oll)))
    ))

(test (&opt-a-b-&rest-x-oll-check-vars :depends-on &opt-a-b-&rest-x-oll-check)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (is (equal '(a c x) (ll-vars oll)))))


(test (&opt-a-b-&rest-x-oll-check-opt-vars :depends-on &opt-a-b-&rest-x-oll-check)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (is (equal '(a c) (mapcar #'llv-name (oll-optional-vars oll))))))

(test (&opt-a-b-&rest-x-oll-check-opt-forms :depends-on &opt-a-b-&rest-x-oll-check)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    ;; (print (oll-rest-var oll))
    ;; (print (ll-default-forms oll))
    (is (equal '((a 2 b) (c 3 d) nil) (ll-default-forms oll)))))

(test (&opt-a-b-&rest-x-oll-check-rest-var :depends-on &opt-a-b-&rest-x-oll-check)
  (with-fixture parse-oll (*&opt-a-c-&rest-x-oll*)
    (is (equal '(x) (mapcar #'llv-name (oll-rest-var oll))))))


(defparameter *a-b-&key-c-d-oll* '(a b &key c d))

(test parse-a-b-&key-c-d-oll
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (pass "Parsing (a b &key c d) ordinary lambda list.")))


(test (is-ordinary-a-b-&key-c-d-oll :depends-on parse-a-b-&key-c-d-oll)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (is-true (ordinary-lambda-list-p oll))))

(test (a-b-&key-c-d-oll-check :depends-on is-ordinary-a-b-&key-c-d-oll)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (is-false (null (oll-ordinary-vars oll)))
    (is-true (null (oll-optional-vars oll)))
    (is-true (null (oll-rest-var oll)))
    (is-false (null (oll-keyword-vars oll)))
    (is-true (null (oll-allow-other-keys oll)))
    (is-true (null (oll-auxiliary-vars oll)))
    ))

(test (a-b-&key-c-d-oll-check-vars :depends-on a-b-&key-c-d-oll-check)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (is (equal '(a b c d) (ll-vars oll)))))

(test (a-b-&key-c-d-oll-check-key-vars :depends-on a-b-&key-c-d-oll-check)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (is (equal '(c d) (mapcar #'llv-name (oll-keyword-vars oll))))))

(test (a-b-&key-c-d-oll-check-key-forms :depends-on a-b-&key-c-d-oll-check)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    ;; (print (oll-rest-var oll))
    ;; (print (ll-default-forms oll))
    (is (equal '(nil nil nil nil) (ll-default-forms oll)))))

(test (a-b-&key-c-d-oll-check-rest-var :depends-on a-b-&key-c-d-oll-check)
  (with-fixture parse-oll (*a-b-&key-c-d-oll*)
    (is-true (null (oll-rest-var oll)))))


;;;; end of file -- clllp-tests.lisp
