;;;; -*- Mode: Lisp -*-

;;;; clllp-package.lisp
;;;; Package definition for the CL Lambda List Parsing library.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(defpackage "IT.UNIMIB.DISCO.MA.COMMON-LISP.LAMBDA-LIST-PARSING"
  (:use "CL")
  (:nicknames
   "CL.EXT.EVALUATION-AND-COMPILATION.LAMBDA-LIST-PARSING"
   "CL.EXT.EAC.LLP"
   "CLLLP"
   "clllp")

  (:export "LAMBDA-LIST-TYPE")

  (:export
   "LAMBDA-LIST-VAR-TYPE"

   "ORDINARY-LAMBDA-LIST-VAR-TYPE"
   "GENERIC-FUNCTION-LAMBDA-LIST-VAR-TYPE"
   ;; ... add other ones ...

   )

  (:export
   "LL-P"
   "LL-ORDINARY-VARS"
   "LL-OPTIONAL-VARS"
   "LL-KEYWORD-VARS"
   "LL-REST-VAR"
   "LL-AUXILIARY-VARS"
   "LL-ALLOW-OTHER-KEYS"
   )
  (:export
   "LAMBDA-LIST-ITEM"
   "LLI-P"
   "MKLLITEM"
   "LLI-ITEM"
   "LLI-KIND"
   "LLI-FORM"
   )
  (:export
   "LAMBDA-LIST-VAR"
   "LLV-P"
   "MKLLVAR"
   "LLV-ITEM"
   "LLV-KIND"
   "LLV-FORM"
   )
  (:export "LLI-NAME" "LLV-NAME")

  (:export "ORDINARY-LAMBDA-LIST")
  (:export "SPECIALIZED-LAMBDA-LIST")
  (:export "GENERIC-FUNCTION-LAMBDA-LIST")
  (:export "DESTRUCTURING-LAMBDA-LIST")
  (:export "MACRO-LAMBDA-LIST"
   "MACRO-LAMBDA-LIST-WHOLE-VAR"
   "MACRO-LAMBDA-LIST-ENV-VAR"
   "MACRO-LAMBDA-LIST-BODY-VAR"
   )

  (:export "PARSE-LL" "PARSE-LAMBDA-LIST")
  
  (:export "LL-VARS")
  

  (:documentation
   "The CL.EXT.EVALUATION-AND-COMPILATION.LAMBDA-LIST-PARSING Package.

This package contains a stand-alone parser for Common Lisp Lambda Lists
(of various kinds).  The parser recognizes the standard lambda list
kinds described in the ANSI standard.  It is also somewhat extensible
to accommodate other kinds of lambda lists that may be provided by
some DSL built on top of Common Lisp.")
  )

;;;; end of file -- clllp-package.lisp
