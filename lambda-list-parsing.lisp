;;;; -*- Mode: Lisp -*-

;;;; lambda-list-parsing.lisp --
;;;;
;;;; Parsing Lambda Lists according to the ANSI specification Section 3.3
;;;  "Lambda Lists".
;;;;
;;;; See file COPYING copyright and licensing information.


(in-package "CLLLP")


;;;;===========================================================================
;;;; Types and Classes.

(deftype lambda-list-type ()
  '(member :ordinary
           :generic-function
           :specialized
           :macro
           :destructuring
           :boa
           :defsetf
           :deftype
           :define-modify-macro
           :define-method-combination
           ))


(deftype ordinary-lambda-list-var-type ()
  '(member &reqvar
           &optional
           &rest
           &key
           &allow-other-keys
           &aux))


(deftype generic-function-lambda-list-var-type ()
  '(member &reqvar
           &optional
           &rest
           &key
           &allow-other-keys
           ;; No &aux.
           ))


(deftype specialized-lambda-list-var-type ()
  'ordinary-lambda-list-var-type)


(deftype macro-lambda-list-var-type ()
  '(or (member &whole &environment &body)
       ordinary-lambda-list-var-type))


(deftype destructuring-lambda-list-var-type ()
  '(or (member &whole &body)
       ordinary-lambda-list-var-type))


(deftype boa-lambda-list-var-type ()
  'ordinary-lambda-list-var-type)


(deftype defsetf-lambda-list-var-type ()
  '(or (member &environment)
       generic-function-lambda-list-var-type ; Seems right.
       ))


(deftype deftype-lambda-list-var-type ()
  'macro-lambda-list-var-type)


(deftype define-modify-macro-lambda-list-var-type ()
  '(member &reqvar &optional &rest))


(deftype define-method-combination-lambda-list-var-type ()
  '(or (member &whole)
       generic-function-lambda-list-var-type ; Seems right.
       ))


(deftype lambda-list-var-type ()
  '(member &reqvar ; Just for symmetry.
           &whole
           &environment
           &optional
           &rest
           &body
           &key
           &allow-other-keys
           &aux
           ))


(defstruct (t_lambda-list
            (:conc-name ll-)
            (:constructor nil)
            (:copier nil))
  (ordinary-vars () :type list :read-only t)
  (optional-vars () :type list :read-only t)
  (rest-var () :type list :read-only t)
  (keyword-vars () :type list :read-only t)
  (allow-other-keys nil :type boolean :read-only t)
  (auxiliary-vars () :type list :read-only t)
  )


(defstruct (lambda-list-item
            (:conc-name lli-)
            (:constructor mkllitem (item kind &optional (form item))))
  "The Lambda List Item Structure.

Instances of this structure represent (i.e., they are the parse
result) 'items' in a lambda list.  Since these items can be used for
macro lambda lists, they must accommodate destructuring.  An item has
some content held in the slot ITEM, a KIND, which is one of the
standard lambda-list modifiers (&key, &rest, etc, plus &reqvar to
denote 'required' items) and a possible FORM holding the overall item
definition in the original lambda-list (e.g., the optional default in
optional parameters)."
  (item nil :type (or symbol t_lambda-list list) :read-only t)
  (kind '&reqvar :type lambda-list-var-type :read-only t)
  (form () :type (or symbol cons) :read-only t)
  )


(defstruct (lambda-list-var
            (:include lambda-list-item)
            (:conc-name llv-)
            (:constructor mkllvar (item kind &optional (form item))))
  )


(declaim (inline lli-name llv-name))

(defun lli-name (lli)
  (declare (type lambda-list-item lli))
  (lli-item lli))


(defun llv-name (lli)
  (declare (type lambda-list-var lli))
  (llv-item lli))


(defstruct (ordinary-lambda-list
            (:include t_lambda-list)
            (:conc-name oll-)
            (:constructor
             make-ordinary-lambda-list (&optional
                                        ordinary-vars
                                        optional-vars
                                        rest-var
                                        keyword-vars
                                        allow-other-keys
                                        auxiliary-vars
                                        )))
  "The Ordinary Lambda List Structure.")


(defstruct (specialized-lambda-list
            (:include t_lambda-list)
            (:constructor
             make-specialized-lambda-list (&optional
                                           ordinary-vars
                                           optional-vars
                                           rest-var
                                           keyword-vars
                                           allow-other-keys
                                           auxiliary-vars
                                           )))
  "The Specialized Lambda List Structure.")


(defstruct (generic-function-lambda-list
            (:include t_lambda-list)
            (:constructor
             make-generic-function-lambda-list (&optional
                                                ordinary-vars
                                                optional-vars
                                                rest-var
                                                keyword-vars
                                                allow-other-keys
                                                auxiliary-vars
                                                )))
  "The Generic Function Lambda List Structure.")


(defstruct (destructuring-lambda-list
            (:include t_lambda-list)
            (:constructor
             make-destructuring-lambda-list (&optional
                                             ordinary-vars
                                             optional-vars
                                             rest-var
                                             keyword-vars
                                             allow-other-keys
                                             auxiliary-vars
                                             )))
  "The Destructuring Lambda List Structure.")


(defstruct (macro-lambda-list
            (:include destructuring-lambda-list)
            (:constructor
             make-macro-lambda-list (&optional
                                     whole-var
                                     env-var
                                     ordinary-vars
                                     optional-vars
                                     rest-var
                                     body-var
                                     keyword-vars
                                     allow-other-keys
                                     auxiliary-vars
                                     )))
  "The Macro Lambda List Structure."
  (whole-var () :type list :read-only t)
  (env-var () :type list :read-only t)
  (body-var () :type list :read-only t)
  )


;;;;============================================================================
;;;; Lambda List Parsing.
;;;;
;;;; There are essentially two types of lambda lists: destructuring and not.
;;;; The implementation choice is thus to have two main "parsing
;;;; functions" returning multiple values.
  
(defgeneric parse-ll (lltype ll)
  (:documentation "Parse a Common Lisp Lambda List.

The LLTYPE is a keyword indicating which kind of Lambda List is to be
parsed. LL is the actual lambda list to be parsed (a CONS)."))


(defmethod parse-ll ((lltype (eql :ordinary)) ll)
  "Parsing method for ordinary lambda lists."
  (multiple-value-bind (wholevar
                        envvar
                        reqvars
                        optvars
                        restvar
                        bodyvar
                        keyvars
                        allow-other-keys
                        auxvars)
      (pll lltype ll nil)
    (declare (ignore wholevar envvar bodyvar))
    (make-ordinary-lambda-list reqvars
                               optvars
                               restvar
                               keyvars
                               (not (null allow-other-keys))
                               auxvars
                               )))


(defmethod parse-ll ((lltype (eql :specialized)) ll)
  "Parsing method for specialized (method) lambda lists."
  (multiple-value-bind (wholevar
                        envvar
                        reqvars
                        optvars
                        restvar
                        bodyvar
                        keyvars
                        allow-other-keys
                        auxvars)
      (pll lltype ll nil)
    (declare (ignore wholevar envvar bodyvar))
    (make-specialized-lambda-list reqvars
                                  optvars
                                  restvar
                                  keyvars
                                  (not (null allow-other-keys))
                                  auxvars
                                  )))


(defmethod parse-ll ((lltype (eql :generic-function)) ll)
  "Parsing method for generic functions' lambda lists."
  (multiple-value-bind (wholevar
                        envvar
                        reqvars
                        optvars
                        restvar
                        bodyvar
                        keyvars
                        allow-other-keys
                        auxvars)
      (pll lltype ll nil)
    (declare (ignore wholevar envvar bodyvar))
    (make-specialized-lambda-list reqvars
                                  optvars
                                  restvar
                                  keyvars
                                  (not (null allow-other-keys))
                                  auxvars
                                  )))


(defmethod parse-ll ((lltype (eql :destructuring)) ll)
  "Parsing method for destructuring lambda lists."
  (multiple-value-bind (wholevar
                        envvar
                        reqvars
                        optvars
                        restvar
                        bodyvar
                        keyvars
                        allow-other-keys
                        auxvars)
      (pll lltype ll t)
    (declare (ignore wholevar envvar bodyvar))
    (make-destructuring-lambda-list reqvars
                                    optvars
                                    restvar
                                    keyvars
                                    (not (null allow-other-keys))
                                    auxvars
                                    )))


(defmethod parse-ll ((lltype (eql :macro)) ll)
  "Parsing method for macro lambda lists."
  (multiple-value-bind (wholevar
                        envvar
                        reqvars
                        optvars
                        restvar
                        bodyvar
                        keyvars
                        allow-other-keys
                        auxvars)
      (pll lltype ll t)
    (make-macro-lambda-list wholevar
                            envvar
                            reqvars
                            optvars
                            restvar
                            bodyvar
                            keyvars
                            (not (null allow-other-keys))
                            auxvars
                            )))


;;; pll --
;;; The actual work-horse.
;;; Let's try a different implementation than what tried previously.
;;;
;;; First we need to define the "follow" sets; we are still doing a
;;; recursive descent parser, but they are useful...

(defparameter *following-lambda-list-keywords*
  (make-hash-table :test #'equal))


(eval-when (:load-toplevel :execute)
  (flet ((set-following-lambda-list-kwd (ll-type
                                         ll-kwd
                                         following-ll-kwds)
           (setf (gethash (list ll-type ll-kwd)
                          *following-lambda-list-keywords*)
                 following-ll-kwds))
         )
    ;; Ordinary Lambda List

    (set-following-lambda-list-kwd
     :ordinary '&reqvar
     '(&optional &rest &key &aux))

    (set-following-lambda-list-kwd
     :ordinary '&optional
     '(&rest &key &aux))

    (set-following-lambda-list-kwd
     :ordinary '&key
     '(&allow-other-keys &aux))

    (set-following-lambda-list-kwd
     :ordinary '&allow-other-keys
     '(&aux))

    ;; Generic Function Lambda List.

    (set-following-lambda-list-kwd
     :generic-function '&reqvar
     '(&optional &rest &key))

    (set-following-lambda-list-kwd
     :generic-function '&optional
     '(&rest &key))

    (set-following-lambda-list-kwd
     :generic-function '&key
     '(&allow-other-keys))

    ;; Specialized Lambda List.
    ;; Just like :ordinary.

    (set-following-lambda-list-kwd
     :specialized '&reqvar
     '(&optional &rest &key &aux))

    (set-following-lambda-list-kwd
     :specialized '&optional
     '(&rest &key &aux))

    (set-following-lambda-list-kwd
     :specialized '&key
     '(&allow-other-keys &aux))

    (set-following-lambda-list-kwd
     :specialized '&allow-other-keys
     '(&aux))


    ;; Macro Lambda List.
    ;; This is the complicated one.

    (set-following-lambda-list-kwd
     :macro '&reqvar
     '(&environment &optional &body &rest &key &aux))

    (set-following-lambda-list-kwd
     :macro '&optional
     '(&environment &body &rest &key &aux))

    (set-following-lambda-list-kwd
     :macro '&key
     '(&environment &allow-other-keys &aux))

    (set-following-lambda-list-kwd
     :macro '&allow-other-keys
     '(&environment &aux))

    (set-following-lambda-list-kwd
     :macro '&aux
     '(&environment))

    ) ; FLET.
    





(defun pll (ll-type ll &optional recur)
  (declare (type lambda-list-type ll-type)
           (type boolean recur))

  (let ((state '&reqvar)
        (wholevar ())
        (reqvars ())
        (optvars ())
        (restvar ())
        (bodyvar ())
        (keyvars ())
        (auxvars ())
        (envvar ())

        (allow-other-keys ())

        (destr-p ll-type #|(if recur
                               :destructuring
                               :non-destructuring)|#)
        )
    (declare (type lambda-list-var-type state)
             (type list wholevar reqvars optvars restvar bodyvar keyvars auxvars envvar)
             (type lambda-list-type destr-p)
             )
    (labels ((start (ll)
               (keep-parsing ll))

             (keep-parsing (ll)
               ;; (format t "||| ~A~%" ll)
               (if (null ll)
                   (finish)
                   (typecase ll
                     (symbol ; dotted-pair &rest-like variable.
                      (push (mkllvar ll '&rest) restvar)
                      (finish))
                     (list
                      ;; (format t ">>> ~A ~A~%" state (first ll))
                      (change-state (first ll) (rest ll))
                      ))
                   ))

             (change-state (v rest-ll)
               (let ((next-v nil))
                 (declare (type boolean next-v))
                 (macrolet ((test-and-change-state (s)
                              `(when (eq v ,s) ; Obviously never true for &reqvar.
                                 (setf state ,s next-v t)))
                            )
                   (or (test-and-change-state '&reqvar)
                       (test-and-change-state '&whole)
                       (test-and-change-state '&environment)
                       (test-and-change-state '&optional)
                       (test-and-change-state '&rest)
                       (test-and-change-state '&key)
                       (test-and-change-state '&allow-other-keys)
                       (test-and-change-state '&aux)
                       (test-and-change-state '&body)
                       )

                   ;; Now, if we have NEXT-V true, we have changed state.
                   ;; If we did, we need to skip the current V, that
                   ;; is, one of the lambda list special symbols, and
                   ;; keep parsing from the rest.  Otherwise we keep
                   ;; parsing from the current V item.

                   ;; (format t "<<< ~A ~A ~A~%" rest-ll state (first rest-ll))

                   (if next-v
                       (next state (first rest-ll) (rest rest-ll))
                       (next state v rest-ll))
                   ))
               )

             (next (s v rest-ll)
               ;; S = current state
               ;; V = first item: a variable or a more complex list.
               ;; REST-LL = what remains of the Lambda List.

               (case s
                 (&reqvar
                  ;; The next test is redundant, but it is there just
                  ;; for symmetry.
                  (unless (typep v '(&optional &rest &key &aux))
                    (etypecase v
                      (symbol (push (mkllvar v '&reqvar) reqvars))
                      (list (push (if recur
                                      (parse-ll destr-p v)
                                      (mkllvar (first v) '&reqvar v))
                                  reqvars))))
                  )

                 (&key
                  (unless (member v '(&allow-other-keys &aux
                  (etypecase v
                    (symbol (push (mkllvar v '&key) keyvars))
                    (list
                     (destructuring-bind (k . iv-ksp)
                         v
                       (typecase k
                         (symbol (push (mkllvar k '&key v) keyvars))
                         (list
                          (push
                           (mkllvar (first k) ; This is what is "visible".
                                    '&key
                                    `((,(first k)
                                       ,(if recur
                                            (if (listp (second k)) 
                                                (parse-ll destr-p (second k))
                                                (parse-ll destr-p (rest k)) ; Faking a Lambda List.
                                                )
                                            (second k)
                                            ))
                                      ,@iv-ksp))
                           keyvars)))))
                    )
                  )

                 (&optional
                  (etypecase v
                    (symbol (push (mkllvar v '&optional) optvars))
                    (list
                     (destructuring-bind (o . iv-osp)
                         v
                       (typecase o
                         (symbol (push (mkllvar o '&optional v) optvars))
                         (list (push 
                                (mkllitem (if recur
                                              (parse-ll destr-p o) 
                                              o)
                                          '&optional
                                          iv-osp)
                                     optvars)))))
                    )
                  )
                
                 (&rest
                  (etypecase v
                    (symbol (push (mkllvar v s) restvar))
                    (list (push (if recur
                                    (parse-ll destr-p v)
                                    (mkllvar (first v) s v))
                                restvar)))
                  )

                 (&body
                  (etypecase v
                    (symbol (push (mkllvar v s) bodyvar))
                    (list (push (if recur
                                    (parse-ll destr-p v)
                                    (mkllvar (first v) s v))
                                bodyvar)))
                  )

                 #|
                 (&rest
                  (etypecase v
                    (symbol (push (mkllvar v s) restvar))
                    (list (push (mkllitem (parse-ll destr-p v) s) restvar)))
                  )

                 (&body
                  (etypecase v
                    (symbol (push (mkllvar v s) bodyvar))
                    (list (push (mkllitem (parse-ll destr-p v) s) bodyvar)))
                  )
                 |#

                 (&whole
                  (push (mkllvar v '&whole) wholevar))

                 (&environment
                  (push (mkllvar v '&environment) envvar))

                 (&aux
                  (typecase v
                    (symbol (push (mkllvar v '&aux) auxvars))
                    (list (push (mkllvar (first v) '&aux v) auxvars))))

                 (&allow-other-keys
                  (pushnew t allow-other-keys))
                 )

               (keep-parsing rest-ll))

             (finish ()
               (values wholevar
                       envvar
                       (nreverse reqvars)
                       (nreverse optvars)
                       restvar
                       bodyvar
                       (nreverse keyvars)
                       allow-other-keys
                       (nreverse auxvars)
                       ))

             )
      (start ll)
      )))


;;;---------------------------------------------------------------------------
;;; Utilities.
;;; Note that LL-VARS and LL-DEFAULT-FORMS are always guaranteed to be
;;; "usable" with PAIRLIS; "default forms" for required variables, specialized
;;; variables and &rest variables will appear positionally in the result of
;;; LL-DEFAULT-FORMS as NIL.


;;; ll-vars

(defgeneric ll-vars (ll)
  (:documentation "Returns the variables named in a Lambda List."))


(defmethod ll-vars ((ll t_lambda-list))
  (nconc (mapcar #'lli-name (ll-ordinary-vars ll))
         (mapcar #'lli-name (ll-optional-vars ll))
         (mapcar #'lli-name (ll-rest-var ll))
         (mapcar #'lli-name (ll-keyword-vars ll))
         (mapcar #'lli-name (ll-auxiliary-vars ll)))
  )


(defmethod ll-vars ((lli lambda-list-item))
  (lli-name lli))


(defmethod ll-vars ((ll destructuring-lambda-list))
  (nconc (mapcar #'ll-vars (ll-ordinary-vars ll))
         (mapcar #'ll-vars (ll-optional-vars ll))
         (mapcar #'ll-vars (ll-rest-var ll))
         (mapcar #'ll-vars (ll-keyword-vars ll))
         (mapcar #'ll-vars (ll-auxiliary-vars ll)))
  )


(defmethod ll-vars ((ll macro-lambda-list))
  (nconc (mapcar #'ll-vars (macro-lambda-list-whole-var ll))
         (mapcar #'ll-vars (macro-lambda-list-env-var ll))
         (mapcar #'ll-vars (ll-ordinary-vars ll))
         (mapcar #'ll-vars (ll-optional-vars ll))
         (mapcar #'ll-vars (ll-rest-var ll))
         (mapcar #'ll-vars (macro-lambda-list-body-var ll))
         (mapcar #'ll-vars (ll-keyword-vars ll))
         (mapcar #'ll-vars (ll-auxiliary-vars ll)))
  )


;;; ll-default-forms

(defgeneric ll-default-forms (ll))

(defmethod ll-default-forms ((lli lambda-list-item)) ; Base case.
  (let ((item-kind (lli-kind lli)))
    (unless (or (eq '&rest item-kind)
                (eq '&reqvar item-kind)) ; This excludes specialized vars too.
      (lli-form lli))))


(defmethod ll-default-forms ((ll t_lambda-list))
  (nconc (mapcar #'ll-default-forms (ll-ordinary-vars ll))
         (mapcar #'ll-default-forms (ll-optional-vars ll))
         (mapcar #'ll-default-forms (ll-rest-var ll))
         (mapcar #'ll-default-forms (ll-keyword-vars ll))
         (mapcar #'ll-default-forms (ll-auxiliary-vars ll))
         ))


(defmethod ll-default-forms ((ll macro-lambda-list))
  (nconc (mapcar #'ll-default-forms (macro-lambda-list-whole-var ll))
         (mapcar #'ll-default-forms (macro-lambda-list-env-var ll))
         (mapcar #'ll-default-forms (ll-ordinary-vars ll))
         (mapcar #'ll-default-forms (ll-optional-vars ll))
         (mapcar #'ll-default-forms (ll-rest-var ll))
         (mapcar #'ll-default-forms (macro-lambda-list-body-var ll))
         (mapcar #'ll-default-forms (ll-keyword-vars ll))
         (mapcar #'ll-default-forms (ll-auxiliary-vars ll))
         ))



;;; count-ll-vars

(defgeneric count-ll-vars (kind lambda-list))


(defmethod count-ll-vars ((kind symbol) (lli lambda-list-item))
  1)


(defmethod count-ll-vars ((kind (eql '&reqvar)) (ll t_lambda-list))
  (list-length (ll-ordinary-vars ll)))

(defmethod count-ll-vars ((kind (eql '&optional)) (ll t_lambda-list))
  (list-length (ll-optional-vars ll)))

(defmethod count-ll-vars ((kind (eql '&rest)) (ll t_lambda-list))
  (if (ll-rest-var ll) 1 0))

(defmethod count-ll-vars ((kind (eql '&key)) (ll t_lambda-list))
  (list-length (ll-keyword-vars ll)))

(defmethod count-ll-vars ((kind (eql '&aux)) (ll t_lambda-list))
  (list-length (ll-auxiliary-vars ll)))

(defmethod count-ll-vars ((kind (eql '&whole)) (ll t_lambda-list)) 0)

(defmethod count-ll-vars ((kind (eql '&environment)) (ll t_lambda-list)) 0)

(defmethod count-ll-vars ((kind (eql '&body)) (ll t_lambda-list)) 0)



(defmethod count-ll-vars ((kind (eql '&reqvar)) (ll destructuring-lambda-list))
  (reduce #'+ (mapcar #'(lambda (lli)
                          (count-ll-vars '&reqvar lli))
                      (ll-ordinary-vars ll))))

(defmethod count-ll-vars ((kind (eql '&optional)) (ll destructuring-lambda-list))
  (reduce #'+ (mapcar #'(lambda (lli)
                          (count-ll-vars '&optional lli))
                      (ll-optional-vars ll))))

(defmethod count-ll-vars ((kind (eql '&rest)) (ll destructuring-lambda-list))
  (reduce #'+ (mapcar #'(lambda (lli)
                          (count-ll-vars '&rest lli))
                      (ll-rest-var ll))))

(defmethod count-ll-vars ((kind (eql '&key)) (ll destructuring-lambda-list))
  (reduce #'+ (mapcar #'(lambda (lli)
                          (count-ll-vars '&key lli))
                      (ll-keyword-vars ll))))

(defmethod count-ll-vars ((kind (eql '&aux)) (ll destructuring-lambda-list))
  (reduce #'+ (mapcar #'(lambda (lli)
                          (count-ll-vars '&aux lli))
                      (ll-auxiliary-vars ll))))



(defmethod count-ll-vars ((kind (eql '&whole)) (ll macro-lambda-list))
  (if (macro-lambda-list-whole-var ll) 1 0))

(defmethod count-ll-vars ((kind (eql '&environment)) (ll macro-lambda-list))
  (list-length (macro-lambda-list-env-var ll)))

(defmethod count-ll-vars ((kind (eql '&body)) (ll macro-lambda-list))
  (if (macro-lambda-list-body-var ll) 1 0))


;;;; end of file -- lambda-list-parsing.lisp --
