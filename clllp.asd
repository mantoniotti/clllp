;;;; -*- Mode: Lisp -*-

;;;; clllp.asd
;;;; System definition for the CL Lambda List Parsing library.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(asdf:defsystem "clllp"
  :author "Marco Antoniotti"
  :components ((:file "clllp-package")
               (:file "lambda-list-parsing"
                :depends-on ("clllp-package")))
  )

;;;; end of file -- clllp.asd
